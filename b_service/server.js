const express = require("express");

const homeRouter = require("./routes/index");

const PORT = 3000;
const HOST_NAME = "localhost";

const app = express();
app.use(express.static("client"));
app.use(express.urlencoded({ extended: true }));

app.use("/", homeRouter);

app.listen(PORT, () => {
    console.log(`Server running at ${HOST_NAME}:${PORT}`);
});
