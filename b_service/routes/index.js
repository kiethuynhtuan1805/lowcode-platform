const express = require("express");
const properties = require("../package.json");

const homeRoute = express.Router();

homeRoute.get("/", (req, res) => {
    const aboutInfo = {
        name: properties.name,
        description: properties.description,
        author: properties.author
    };
    res.sendFile(__dirname, + "../client/index.html")   
    res.json(aboutInfo);
});

module.exports = homeRoute;
